import json
import boto3

ec2=boto3.resource("ec2")

def lambda_handler(event, context):
    instanceid= event["detail"]["instance-id"]
    if ec2.Instance(instanceid).tags == '' or not ec2.Instance(instanceid).tags:
        ec2.Instance(instanceid).terminate()
        print("Terminating instance " + instanceid + " for violating enforced tagging policy")